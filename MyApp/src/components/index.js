import {StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import axios from 'axios';
import Icon from 'react-native-vector-icons/Feather'

class TodoListComponent extends React.Component {

state={
  todolist:[]
}

  componentDidMount(){
    axios.get('http://code.aldipee.com/api/v1/todos').then(responseData => {
      // console.log(responseData.data.results)

      this.setState({todolist: responseData.data.results})
      })

    };

  render() {
    return (
      <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}> Aplikasi TodoList </Text>
        
      </View>
      
      <View style={styles.content}>  

        {this.state.todolist.map(itemtodo => {
          return <Text style={styles.item}> {itemtodo.title} </Text>
        })}

        <Text style={styles.item}> Item Todo List 2 </Text>
      </View>
    </View>
    );
  }
}


// const TodoListComponent = () => {
  

//   // componentDidMount(){
//   //   axios.get('http://code.aldipee.com/api/v1/todos').then(responseData => {
//   //     console.log(responseData.data)
//   //     })
//   //   };
  
//   return (
//     <View style={styles.container}>
//       <View style={styles.header}>
//         <Text style={styles.title}> Aplikasi TodoList </Text>
//       </View>
      
//       <View style={styles.content}>  
//         <Text style={styles.item}> Item Todo List 1 </Text>
//         <Text style={styles.item}> Item Todo List 2 </Text>
//       </View>
//     </View>
//     );
//   };
  

export default TodoListComponent;

const styles = StyleSheet.create({

    container :{
        flex: 1,
        backgroundColor: '#fff',
    },

    content : {
        paddingHorizontal: 150,
        paddingVertical: 30,
        fontSize: 17,
        marginTop: 10,
    },

    header: {
        backgroundColor: 'brown',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10,
    },

    title: {
        fontSize: 20,
        color: '#fff',
        fontWeight: 'bold',
    },

    item: {
        fontSize: 13,
        paddingVertical: 7,
        paddingHorizontal: 5,
        marginVertical: 5,
        color: 'black',
        borderColor: '#d4d4d4',
        borderWidth: 1,
    },
});
