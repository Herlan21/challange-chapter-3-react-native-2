import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Share
} from 'react-native';
import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {AirbnbRating} from 'react-native-ratings';

function DetailScreen({route, navigation}) {
  const [data, setData] = useState([]);
  console.log(data.title);
  const {id} = route.params;
  // ! SHARE

  const onShare = async () => {
    try {
      const result = await Share.share({
        message:
          'React Native | A framework for building native apps using React',
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
    
  };

  // !! API

  useEffect(() => {
    let isMounted = true;
    axios.get(`http://code.aldipee.com/api/v1/movies/${id}`).then(res => {
      console.log(res.data);
      setData(res.data);
    });

    return () => {
      isMounted = false;
    };
  }, []);


  return (
    <ScrollView contentContainerStyle={{paddingBottom: 5000}}>
      <View>
        <View>
          <Image
            style={styles.gambarPoster}
            source={{uri: data.backdrop_path}}
          />
        </View>

        {/* //!! TOMBOL BACK */}
        <TouchableOpacity
          style={{position: 'absolute'}}
          onPress={() => navigation.goBack()}>
          <Text style={styles.tombolBack}>  ◀ </Text>
        </TouchableOpacity>

        <View style={styles.kotak}>
          <Image style={styles.gambarkotak} source={{uri: data.poster_path}} />
          <View style={{top: -1, left: 8}}>
            <View>
              <Text style={styles.judulKotak}>{data.title}</Text>

              {/* //!RATING */}
              <AirbnbRating
                count={9}
                showRating={false}
                isDisabled
                defaultRating={data.vote_average}
                size={16}
                ratingContainerStyle={{
                  marginLeft: -140,
                  marginTop: 5,
                }}
              />

              <Text style={styles.judulKotak}>{data.vote_average}</Text>
              <Text style={styles.judulKotak}>{data.runtime}</Text>
              <Text style={styles.judulKotak}>{data.tagline}</Text>
              <Text style={styles.judulKotak}>{data.status}</Text>
            </View>

            <View>
              <Text style={styles.genre}>Genre</Text>
            </View>

            {/* // !GENRE */}
            <View>
              <TouchableOpacity
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                {data.genres?.map(item => (
                  <Text key={item.id} style={styles.tombolDetail}>
                    {item.name}
                  </Text>
                ))}
              </TouchableOpacity>
            </View>

            {/* //!HEADER SINOPSIS */}
            <View>
              <Text style={styles.tekssinopsis}>Synopshis</Text>

              {/* //! ISI SINOPSIS */}
              <Text style={styles.overview}> {data.overview} </Text>
            </View>

            {/* //! HEADER AKTOR */}
            <View>
              <Text style={styles.aktor}>Actor / Artist</Text>
            </View>

            {/* // ! NAMA AKTOR */}
            <View style={styles.fotoaktor}>
              {data.credits?.cast.map(item => (
                <View>
                  <View style={{}}>
                    <Text
                      style={{
                        color: '#000',
                        fontFamily: 'sans-serif-condensed',
                        width: 100,
                        textAlign: 'center',
                      }}>
                      {item.name}
                    </Text>
                  </View>

                  {/* //! FOTO AKTOR */}
                  <View style={{}}>
                    <Image
                      style={styles.gambarAktor}
                      key={item.id}
                      source={{uri: item.profile_path}}
                    />
                  </View>
                </View>
              ))}
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

export default DetailScreen;

const styles = StyleSheet.create({
  tombolBack: {
    width: 33,
    height: 30,
    paddingTop: 3,
    fontSize: 16,
    borderRadius: 10,
    color: '#FFF',
    backgroundColor: '#eb3434',
    top: 25,
    left: 20,

    //! SHADOW TOMBOL DETAILS
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.35,
    shadowRadius: 7.5,

    elevation: 12,
  },
  tombolDetail: {
    width: 80,
    height: 35,
    paddingTop: 5,
    borderRadius: 10,
    fontSize: 16,
    color: '#FFF',
    backgroundColor: '#eb3434',
    textAlign: 'center',
    fontFamily: 'sans-serif-condensed',
    left: -120,
    top: 5,

    //! SHADOW TOMBOL DETAILS
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.35,
    shadowRadius: 7.5,

    elevation: 12,
  },

  gambarPoster: {
    width: '100%',
    height: 200,
  },

  kotak: {
    backgroundColor: '#353837',
    width: 350,
    height: 160,
    top: -80,
    alignSelf: 'center',
    borderRadius: 10,
    flexDirection: 'row',

    // ? SHADOW
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,

    elevation: 9,
  },

  gambarkotak: {
    width: 100,
    height: 140,
    marginTop: 10,
    marginLeft: 10,
    borderRadius: 10,
  },

  judulKotak: {
    color: '#FFF',
    fontFamily: 'sans-serif-condensed',
    marginLeft: 10,
    marginTop: 2,
    width: 230,
    flexWrap: 'wrap',
  },

  genre: {
    fontFamily: 'sans-serif-condensed',
    color: '#000',
    left: -130,
    marginTop: 20,
    fontSize: 20,
    fontWeight: '700',
  },

  genrefilm: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    left: -150,
    marginTop: 10,
  },

  teksgenre: {
    color: '#FFF',
    backgroundColor: '#353837',
    fontFamily: 'sans-serif-condensed',
    width: 60,
    height: 30,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },

  tekssinopsis: {
    color: 'black',
    fontFamily: 'sans-serif-condensed',
    fontWeight: '700',
    fontSize: 20,
    left: -130,
    marginTop: 15,
  },

  overview: {
    color: 'black',
    fontFamily: 'sans-serif-condensed',
    fontWeight: '500',
    fontSize: 15,
    left: -130,
  },

  aktor: {
    color: 'black',
    fontFamily: 'sans-serif-condensed',
    fontWeight: '700',
    fontSize: 20,
    left: -130,
    marginTop: 20,
  },

  gambarAktor: {
    width: 100,
    height: 150,
    marginBottom: 30,
    borderRadius: 10,
  },

  fotoaktor: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    left: -120,
    marginBottom: 10,
    alignItems: 'center',
  },
});
