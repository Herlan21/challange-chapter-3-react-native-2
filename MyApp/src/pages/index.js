import SplashScreen from './SplashScreen'
import HomeScreen from './HomeScreen'
import DetailScreen from './DetailScreen'

export {SplashScreen, HomeScreen, DetailScreen}