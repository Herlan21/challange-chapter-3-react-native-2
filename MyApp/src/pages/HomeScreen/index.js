import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  navigation,
} from 'react-native';
import React from 'react';
import axios from 'axios';
import {Rating, AirbnbRating} from 'react-native-ratings';

class HomeScreen extends React.Component {
  state = {
    listfilm: [],
  };

  // ! MEMANGGIL API
  componentDidMount() {
    axios.get('http://code.aldipee.com/api/v1/movies').then(responseData => {
      this.setState({listfilm: responseData.data.results});
    });
  }

  render() {
    const {navigation} = this.props;

    return (
      // ! HEADER
      <ScrollView>
        <View>
          <View style={styles.header}>
            <Text style={styles.title}> Recommended </Text>
          </View>

          {/* //! MENAMPILKAN API */}
          {/* //!RECOMENDED  */}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View>
              <View style={styles.poster}>
                {this.state.listfilm.map(titlefilm => {
                  return (
                    <TouchableOpacity style={styles.film}>
                      <Image
                        key={titlefilm.id}
                        style={styles.gambarPoster}
                        source={{uri: titlefilm.poster_path}}
                      />
                    </TouchableOpacity>
                  );
                })}
              </View>
            </View>
          </ScrollView>

          {/* //! LATEST UPLOAD */}
          <View>
            <Text style={styles.latest}>Latest Upload</Text>
          </View>

          {/* //! POSTER  */}
          <View>
            <View style={{position: 'relative'}}>
              {this.state.listfilm.map(titlefilm => {
                return (
                  <View style={styles.gambarlatest}>
                    <View style={styles.daftarFilm}>
                      <Image
                        style={styles.gambarPoster}
                        source={{uri: titlefilm.poster_path}}
                      />

                      <View>
                        <Text style={styles.judulfilm}>
                          {' '}
                          {titlefilm.title}{' '}
                        </Text>

                        <View>
                          <AirbnbRating
                            count={9}
                            showRating={false}
                            isDisabled
                            defaultRating={titlefilm.vote_average}
                            size={16}
                            ratingContainerStyle={{
                              marginLeft: 10,
                              top: 20,
                            }}
                          />
                        </View>

                        <View>
                          <Text style={styles.keterangan}>
                            {titlefilm.release_date}
                          </Text>

                          <Text style={{ color: '#000', marginLeft: 30 }}>
                            {titlefilm.vote_average}
                          </Text>
                        </View>

                        {/* //! KE HALAMAN DETAILS  */}
                        <View style={styles.containerTombol}>
                          <TouchableOpacity
                            style={{position: 'absolute'}}
                            onPress={() =>
                              navigation.navigate('Detail', {
                                id: titlefilm.id,
                              })
                            }>
                            <Text style={styles.tombolDetail}>Details</Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default HomeScreen;

const styles = StyleSheet.create({
  title: {
    color: 'black',
    fontWeight: 'bold',
    fontFamily: 'sans-serif-condensed',
    fontSize: 20,
    paddingLeft: 13,
  },

  header: {
    paddingVertical: 10,
  },

  gambarPoster: {
    width: 150,
    height: 250,
    borderRadius: 10,
  },

  daftarFilm: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 10,
    marginRight: 12,

    //? SHADOW LATEST UPLOAD
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },

  poster: {
    flexDirection: 'row',
    paddingRight: 12,
    paddingLeft: 12,
  },

  film: {
    marginRight: 15,
  },

  judulfilm: {
    fontSize: 25,
    color: 'black',
    fontWeight: '400',
    width: 240,
    left: 5,
    paddingLeft: 10,
    paddingBottom: 8,
    paddingRight: 10,
    paddingTop: 10,
  },

  keterangan: {
    color: '#000',
    paddingLeft: 10,
    fontSize: 15,
    paddingBottom: 5,
    left: 15,
    top: -30,
    fontFamily: 'sans-serif-condensed',
  },

  latest: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: 'sans-serif-condensed',
    fontSize: 20,
    paddingLeft: 13,
    paddingVertical: 12,
  },

  gambarlatest: {
    marginBottom: 15,
    marginLeft: 13,
  },

  tombolDetail: {
    width: 80,
    height: 35,
    paddingTop: 5,
    borderRadius: 10,
    fontSize: 16,
    color: '#FFF',
    backgroundColor: '#eb3434',
    textAlign: 'center',
    fontFamily: 'sans-serif-condensed',
    top: 25,

    //! SHADOW TOMBOL DETAILS
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.35,
    shadowRadius: 7.5,

    elevation: 12,
  },

  containerTombol: {
    alignItems: 'center',
  },
});
