import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  fontFamily,
} from 'react-native';
import React from 'react';
import {useEffect} from 'react';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home')
    }, 3000)
  }, [navigation])

  return (
    <View>
      <Image style={styles.logo} source={require('../../assets/logo.png')} />
      <Text style={styles.footer}>By Herlan Corporation Group</Text>
    </View>
  );
};

export default Splash;

const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
  logo: {
    alignSelf: 'center',
    width: windowWidth,
    top:100,
  },

  footer: {
    color: '#72808A',
    fontWeight: '700',
    top: 200,
    fontFamily: 'sans-serif-condensed',
    textAlign:'center'
  },
});
